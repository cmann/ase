package ase

const FILE_SIGNATURE = "ASEF"
const FORMAT_VERSION = int32(0x00010000)

const COLOR_START = uint16(0x0001)
const GROUP_START = 0xc001
const GROUP_END = 0xc002

const MODE_COLOR = 1
const MODE_GROUP = 2

const STATE_GET_MODE = 1
const STATE_GET_LENGTH = 2
const STATE_GET_NAME = 3
const STATE_GET_MODEL = 4
const STATE_GET_COLOR = 5
const STATE_GET_TYPE = 6

var COLOR_SIZES map[string]int16
var WRITE_COLOR_TYPES map[string]int16

func init() {
	COLOR_SIZES = map[string]int16{
		"CMYK": 4,
		"RGB":  3,
		"LAB":  3,
		"GRAY": 1,
	}

	WRITE_COLOR_TYPES = map[string]int16{
		"global": 0,
		"spot":   1,
		"normal": 2,
	}
}

// var READ_COLOR_TYPES = {
//   0: 'global'
//   1: 'spot',
//   2: 'normal'
// },
