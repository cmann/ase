package ase

import (
	"bytes"
	"encoding/binary"
	"log"
	"strings"
)

type ColorItem struct {
	Name  string        `json:"name"`
	Model string        `json:"model"` //"model": "RGB", "LAB"
	Color []interface{} `json:"color"`
	Type  string        `json:"type"`
}

type AdobeSwatchExchange struct {
	Version string        `json:"version"`
	Groups  []interface{} `json:"groups"`
	Colors  []ColorItem   `json:"colors"`
}

func (item *ColorItem) PutLab(L float64, a float64, b float64) {
	item.Model = "LAB"
	item.Color = []interface{}{
		float32(L / 100),
		float32(a),
		float32(b),
	}
}

func (data AdobeSwatchExchange) Encode() []byte {

	ase := new(bytes.Buffer)
	endianess := binary.BigEndian

	ase.WriteString(FILE_SIGNATURE)

	// ase.writeInt(FORMAT_VERSION)
	binary.Write(ase, endianess, FORMAT_VERSION)

	// ase.writeInt(numberOfSwatches); // number of blocks
	binary.Write(ase, endianess, int32(len(data.Colors)))

	for i := range data.Colors {

		color := data.Colors[i]

		swatch := new(bytes.Buffer)

		// block type
		// (0xc001 ⇒ Group start, 0xc002 ⇒ Group end, 0x0001 ⇒ Color entry)
		//   ase.writeShort(constants.COLOR_START);
		binary.Write(ase, endianess, COLOR_START)

		// Group/Color name
		// 0-terminated string of length (uint16) double-byte characters
		//   swatch.writeShort(color.name.length + 1);
		binary.Write(swatch, endianess, int16(len(color.Name)+1))
		//^^ name field is less than 30 characters

		//   for(j=0; j < color.name.length; j++) {
		for i := range color.Name {
			// ^^ iterates over the rune

			//     swatch.writeShort(color.name.charCodeAt(j));
			binary.Write(swatch, endianess, int16(color.Name[i]))
		}
		//   swatch.writeShort(0); // terminate with 0
		binary.Write(swatch, endianess, int16(0x00))

		// color model - 4*char (CMYK, RGB, LAB or Gray)
		// var model = color.model.length == 4 ? color.model : color.model + " ";
		model := color.Model
		for len(model) < 4 {
			model += " "
		}
		//   swatch.writeUTF8String(model);
		for i := range model {
			binary.Write(swatch, endianess, int8(model[i]))
		}

		//   // color values
		//   // CMYK ⇒ 4*float32 / RGB & LAB ⇒ 3*float32 / Gray ⇒ 1*float32
		for j := 0; j < int(COLOR_SIZES[strings.ToUpper(color.Model)]); j++ {
			//     swatch.writeFloat(color.color[j]);
			binary.Write(swatch, endianess, color.Color[j])
		}

		// color type - 1*int16 (0 ⇒ Global, 1 ⇒ Spot, 2 ⇒ Normal)
		//   swatch.writeShort(constants.WRITE_COLOR_TYPES[color.type]);
		binary.Write(swatch, endianess, WRITE_COLOR_TYPES[color.Type])

		// block length - 1*int32
		// 	ase.writeInt(swatch.offset);
		log.Printf("block_length cap:%v  len: %v\n", int32(swatch.Cap()), int32(swatch.Len()))
		binary.Write(ase, endianess, int32(swatch.Len()))

		// add to ase buffer
		ase.Write(swatch.Bytes())
	}
	return ase.Bytes()
}
